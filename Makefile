CFLAGS = -O3
LDFLAGS = -flto

LIBS = $(shell pkg-config --cflags --libs libusb-1.0)
LIBSGST = $(shell pkg-config --cflags --libs gstreamer-1.0 gstreamer-app-1.0) -lpthread
LIBSX11 = $(shell pkg-config --cflags --libs x11) -lpthread

all: stream-fifo stream-gst stream-x11

stream-fifo: stream.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(LIBS)

stream-gst: stream.c
	$(CC) $(CFLAGS) -DGST=1 $(LDFLAGS) -o $@ $< $(LIBS) $(LIBSGST)

stream-x11: stream.c
	$(CC) $(CFLAGS) -DX11=1 $(LDFLAGS) -o $@ $< $(LIBS) $(LIBSX11)

clean:
	rm -f stream-fifo stream-gst stream-x11
