// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2023 Marek Vasut <marek.vasut+fx3@mailbox.org>
 */

#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <time.h>
#include <unistd.h>

#include <libusb.h>

/* Sync signals mapping on FX3 connector */
#define CFG_FX3_VS			(1 << 24)
#define CFG_FX3_DE			(1 << 25)
#define CFG_FX3_HS			(1 << 26)
#define CFG_FX3_SYNCMASK		\
	(CFG_FX3_HS | CFG_FX3_VS | CFG_FX3_DE)

/* FX3LAFW specifics */
#define FX3_VID				0x04b4
#define FX3_PID				0x00f3
#define FX3_TRANSFERS			8
#define FX3_TRANSFER_SIZE		(1024 * 1024)
#define USB_TIMEOUT			100

/* Protocol commands */
#define CMD_GET_FW_VERSION		0xb0
#define CMD_START			0xb1
#define CMD_GET_REVID_VERSION		0xb2

#define CMD_START_FLAGS_SUPERWIDE_POS	3
#define CMD_START_FLAGS_CLK_CTL2_POS	4
#define CMD_START_FLAGS_WIDE_POS	5
#define CMD_START_FLAGS_CLK_SRC_POS	6

#define CMD_START_FLAGS_CLK_CTL2	(1 << CMD_START_FLAGS_CLK_CTL2_POS)
#define CMD_START_FLAGS_SAMPLE_8BIT	(0 << CMD_START_FLAGS_WIDE_POS)
#define CMD_START_FLAGS_SAMPLE_16BIT	(1 << CMD_START_FLAGS_WIDE_POS)
#define CMD_START_FLAGS_SAMPLE_24BIT    ((0 << CMD_START_FLAGS_WIDE_POS) | (1 << CMD_START_FLAGS_SUPERWIDE_POS))
#define CMD_START_FLAGS_SAMPLE_32BIT    ((1 << CMD_START_FLAGS_WIDE_POS) | (1 << CMD_START_FLAGS_SUPERWIDE_POS))
#define CMD_START_FLAGS_PCLK_RISING	(0 << CMD_START_FLAGS_CLK_SRC_POS)
#define CMD_START_FLAGS_PCLK_FALLING	(1 << CMD_START_FLAGS_CLK_SRC_POS)

struct cmd_start_acquisition {
	uint8_t flags;
	uint8_t sample_delay_h;
	uint8_t sample_delay_l;
};

#if defined(GST)
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#define GST_FPS		60
static GstElement	*appsrc;
static GstClockTime	timestamp;
#endif /* GST */

#if defined(X11)
#include <pthread.h>
#include <sys/param.h>
#include <X11/Xlib.h>
static Display *disp;
static Window window;
static GC gc;
static XImage *image;
#endif

static int pclkpol;
static int vsdetpol;

#if defined(GST) || defined(X11)
/* Parameters */
static uint32_t width;
static uint32_t height;
static int csync;
static uint32_t imagesize;
static uint8_t *pixels;
static uint32_t pfill;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static void swapcpy(void *d, void *s, unsigned int size)
{
	uint32_t *src = (uint32_t *)s;
	uint32_t *dst = (uint32_t *)d;
	uint32_t src1, src2;
	size /= 4;

	while (size--) {
#if defined(X11)
		src1 = ((*src) & 0x00ff0000) >> 16;
		src2 = ((*src) & 0x000000ff) << 16;
		*src &= ~0x00ff00ff;
		*src |= src1 | src2;
#endif
		if (csync == 1) {
			if (*src & CFG_FX3_HS)
				*src |= 0x000080;
			if (*src & CFG_FX3_VS)
				*src |= 0x008000;
			if (*src & CFG_FX3_DE)
				*src |= 0x800000;
		} else if (csync == 2) {
			if (!(*src & CFG_FX3_HS))
				*src |= 0x000080;
			if (!(*src & CFG_FX3_VS))
				*src |= 0x008000;
			if (!(*src & CFG_FX3_DE))
				*src |= 0x800000;
		}
		*dst++ = *src++;
	}
}

static void *thread(void *arg)
{
	for (;;) {
		pthread_mutex_lock(&mutex);
		/* This takes forever, so use threaded update */
#if defined(GST)
		GstBuffer *buffer = gst_buffer_new_wrapped_full(0, (gpointer)pixels, imagesize, 0, imagesize, NULL, NULL);
		GST_BUFFER_PTS(buffer) = timestamp;
		GST_BUFFER_DURATION(buffer) = gst_util_uint64_scale_int(1, GST_SECOND, GST_FPS);
		timestamp += GST_BUFFER_DURATION (buffer);
		gst_app_src_push_buffer(GST_APP_SRC(appsrc), buffer);
#endif
#if defined(X11)
		XPutImage(disp, window, gc, image, 0, 0, 0, 0, width, height);
#endif
	}
}
#else /* FIFO */
static bool inited;
static int fd;
#endif /* GST || X11 */

static uint32_t prev = 0x01000000;

static void rx_callback(struct libusb_transfer *transfer)
{
	uint32_t *buffer = (uint32_t *)transfer->buffer;
	int detected = 0;
	int skip = 0;
	int i = 0;

	/* Lock on signal */
	for (i = 0; i < (transfer->actual_length / sizeof(*buffer)); i++) {
#define CFG_FX3_SYNCMASK		\
	(CFG_FX3_HS | CFG_FX3_VS | CFG_FX3_DE)

		if ((vsdetpol &&
		     ((prev      & CFG_FX3_SYNCMASK) == CFG_FX3_HS) &&
		     ((buffer[i] & CFG_FX3_SYNCMASK) == 0)) ||
		    (!vsdetpol &&
		     ((prev      & CFG_FX3_SYNCMASK) == 0) &&
		     ((buffer[i] & CFG_FX3_SYNCMASK) == CFG_FX3_HS))) {
			detected = 1;
			break;
		}
		prev = buffer[i];
	}

	if (!detected)
		i = 0;

#if defined(GST) || defined(X11)
	int len;
	/* The rest of current frame */
	if (detected) {
		len = MIN((imagesize) - pfill, 4 * i);
		swapcpy(pixels + pfill, transfer->buffer, len);
		pfill += len;
		if (pfill == imagesize) {
			pfill = 0;
			pthread_mutex_unlock(&mutex);
		}
	}

	len = MIN((imagesize) - pfill, transfer->actual_length - 4 * i);
	swapcpy(pixels + pfill, transfer->buffer + 4 * i, len);
	pfill += len;
	if (pfill == imagesize) {
		pfill = 0;
		pthread_mutex_unlock(&mutex);
	}
#else
	if (!inited && detected) {
		skip = i;
		inited = true;
	}
	if (inited)
		write(fd, transfer->buffer + 4 * skip, transfer->actual_length - 4 * skip);
#endif

	libusb_submit_transfer(transfer);
}

int main(int argc, char** argv)
{
	struct cmd_start_acquisition cmd = { .flags = CMD_START_FLAGS_SAMPLE_32BIT };
	const struct libusb_version* version;
	struct libusb_transfer **transfers;
	libusb_device_handle *handle;
	libusb_context *ctx = NULL;
	uint8_t *buffer;
	int i, ret;

#if defined(GST) || defined(X11)
	pthread_t thr;

	if (argc != 6) {
		printf("Usage: %s <width> <height> <pclkpol> <vsdetpol> <colorsync>\n", argv[0]);
		printf("width ....... width of input frame, including HSA HBP HACT HFP\n");
		printf("height ...... height of input frame, including VSA VBP VACT VFP\n");
		printf("pclkpol ..... 0 - PCLK sampled on RISING edge\n");
		printf("              1 - PCLK sampled on FALLING edge\n");
		printf("vsdetpol .... 0 - VSYNC lock on RISING edge\n");
		printf("              1 - VSYNC lock on FALLING edge\n");
		printf("colorsync ... 0 - do not color sync signals in frame\n");
		printf("              1 - color active HIGH sync signals\n");
		printf("              2 - color active LOW sync signals\n");
#if defined(GST)
		printf("GSTFPS=1 environment variable enables frame rate counter overlay\n");
#endif
		return 0;
	}

	width = strtoull(argv[1], NULL, 10);
	height = strtoull(argv[2], NULL, 10);
	pclkpol = strtoull(argv[3], NULL, 10);
	vsdetpol = strtoull(argv[4], NULL, 10);
	csync = strtoull(argv[5], NULL, 10);
	imagesize = sizeof(uint32_t) * width * height;
	pixels = memalign(4096, imagesize);
	if (!pixels)
		return ENOMEM;

	pthread_mutex_lock(&mutex);
	pthread_create(&thr, NULL, thread, NULL);
	pthread_detach(thr);
#endif	/* GST or X11 */

#if defined(GST)
	GstElement *pipeline, *queue, *conv, *fpssink, *videosink, *sink;

	/* init GStreamer */
	gst_init(&argc, &argv);

	/* setup pipeline */
	pipeline = gst_pipeline_new("pipeline");
	appsrc = gst_element_factory_make("appsrc", "source");
	queue = gst_element_factory_make("queue", "queue");
	conv = gst_element_factory_make("videoconvert", "conv");
	videosink = gst_element_factory_make("autovideosink", "videosink");
	sink = videosink;

	if (getenv("GSTFPS")) {
		fpssink = gst_element_factory_make("fpsdisplaysink", "fpssink");
		g_object_set (G_OBJECT(fpssink), "video-sink", videosink, NULL);
		sink = fpssink;
	}

	g_object_set (G_OBJECT(sink), "sync", false, NULL);

	/* setup */
	g_object_set(G_OBJECT(appsrc), "caps",
		     gst_caps_new_simple("video/x-raw",
		     			 "format", G_TYPE_STRING, "RGBx",
					 "width", G_TYPE_INT, width,
					 "height", G_TYPE_INT, height,
					 "framerate", GST_TYPE_FRACTION, GST_FPS, 1,
					 NULL),
		     NULL);
	gst_bin_add_many(GST_BIN (pipeline), appsrc, queue, conv, sink, NULL);
	gst_element_link_many(appsrc, queue, conv, sink, NULL);

	/* setup appsrc */
	g_object_set(G_OBJECT(appsrc),
		     "stream-type", 0,
		     "is-live", TRUE,
		     "format", GST_FORMAT_TIME,
		     NULL);

	/* play */
	gst_element_set_state(pipeline, GST_STATE_PLAYING);
#endif

#if defined(X11)
	XWindowAttributes attr;

	disp = XOpenDisplay(NULL);
	if (!disp)
		return ENODEV;

	window = XCreateSimpleWindow(disp, XDefaultRootWindow(disp),
				     0, 0, width, height, 0, 0, 0);

	gc = XCreateGC(disp, window, 0, NULL);

	XGetWindowAttributes(disp, window, &attr);

	image = XCreateImage(disp, attr.visual, attr.depth, ZPixmap, 0,
			     pixels, width, height, 32,
			     width * sizeof(uint32_t));
	XMapWindow(disp, window);
#endif

#if !defined(GST) && !defined(X11)
	if (argc != 3) {
		printf("Usage: %s <pclkpol> <vsdetpol>\n", argv[0]);
		printf("pclkpol ..... 0 - PCLK sampled on RISING edge\n");
		printf("              1 - PCLK sampled on FALLING edge\n");
		printf("vsdetpol .... 0 - VSYNC lock on RISING edge\n");
		printf("              1 - VSYNC lock on FALLING edge\n");
		return 0;
	}

	pclkpol = strtoull(argv[1], NULL, 10);
	vsdetpol = strtoull(argv[2], NULL, 10);

	fd = open("/tmp/fifo", O_WRONLY);
	printf("fifo=%d\n", fd);
	if (fd < 0)
		return fd;
#endif

	if (pclkpol)
		cmd.flags |= CMD_START_FLAGS_PCLK_FALLING;

	version = libusb_get_version();
	printf("Using libusb v%d.%d.%d.%d\n\n", version->major, version->minor, version->micro, version->nano);
	ret = libusb_init(&ctx);
	if (ret < 0)
		return ret;

	handle = libusb_open_device_with_vid_pid(ctx, FX3_VID, FX3_PID);
	if (handle == NULL) {
		printf("Unable to open device\n");
		return -1;
	}

	ret = libusb_claim_interface(handle, 0);
	if (ret < 0) {
		printf("Unable to claim interface (ret=%d)\n", ret);
		return ret;
	}

	transfers = calloc(FX3_TRANSFERS, sizeof(*transfers));
	if (!transfers) {
		printf("Unable to allocate transfers\n");
		return -ENOMEM;
	}

	buffer = calloc(FX3_TRANSFERS, FX3_TRANSFER_SIZE);
	if (!buffer) {
		printf("Unable to allocate buffer\n");
		return -ENOMEM;
	}

	for (i = 0; i < FX3_TRANSFERS; i++) {
		transfers[i] = libusb_alloc_transfer(0);
		libusb_fill_bulk_transfer(transfers[i], handle, 2 | LIBUSB_ENDPOINT_IN,
					  buffer + i * FX3_TRANSFER_SIZE, FX3_TRANSFER_SIZE,
					  rx_callback, NULL, USB_TIMEOUT);
		ret = libusb_submit_transfer(transfers[i]);
		if (ret) {
			printf("Unable to submit bulk read transfer %d (ret=%d, %s)\n",
				i, ret, libusb_error_name(ret));
			return ret;
		}
	}

	ret = libusb_control_transfer(handle,
			LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_OUT,
			CMD_START, 0x0000, 0x0000,
			(unsigned char *)&cmd, sizeof(cmd), USB_TIMEOUT);
	if (ret < 0) {
		printf("Unable to send start command (ret=%d, %s)\n",
			ret, libusb_error_name(ret));
		return ret;
	}

	for (;;) {
		ret = libusb_handle_events_completed(ctx, NULL);
		if (ret < 0) {
			printf("Unable to handle events (ret=%d, %s)\n",
				ret, libusb_error_name(ret));
			return ret;
		}
	}

	for (i = 0; i < FX3_TRANSFERS; i++)
		libusb_free_transfer(transfers[i]);

	libusb_release_interface(handle, 0);
	libusb_close(handle);
	libusb_exit(ctx);

	return 0;
}
